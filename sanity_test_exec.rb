# frozen_string_literal: true

require 'google/cloud/storage'
require './sanity_test'

# Functions to execute License DB Sanity Test
module LicenseDBSanityTestExec
  def self.directories_are_of_expected_size(bucket_name, expected_sizes)
    bucket = LicenseDBSanityTest.connect_to_bucket(Google::Cloud::Storage.anonymous, bucket_name)
    LicenseDBSanityTest.expected_vs_actual_dir_sizes(bucket, expected_sizes)
  end

  def self.timestamp_within_past_two_days(bucket_name)
    bucket = LicenseDBSanityTest.connect_to_bucket(Google::Cloud::Storage.anonymous, bucket_name)
    timestamp = LicenseDBSanityTest.get_latest_timestamp(bucket)
    LicenseDBSanityTest.datetime_within_last_x_days?(timestamp, 2)
  end

  def self.expected_number_of_rows(bucket_name, expected_rows)
    bucket = LicenseDBSanityTest.connect_to_bucket(Google::Cloud::Storage.anonymous, bucket_name)
    LicenseDBSanityTest.row_count_is_at_least_expected?(bucket, expected_rows)
  end

  def self.subset_of_licenses_are_present(bucket_name, expected_subset_licenses)
    bucket = LicenseDBSanityTest.connect_to_bucket(Google::Cloud::Storage.anonymous, bucket_name)
    LicenseDBSanityTest.licenses_are_present?(bucket, expected_subset_licenses)
  end
end
