# frozen_string_literal: true

require_relative '../sanity_test'

RSpec.describe LicenseDBSanityTest do
  describe '.connect_to_bucket' do
    let(:storage) { double('storage') }
    let(:bucket_name) { 'my_bucket' }
    let(:bucket) { double('bucket') }

    describe 'connecting to an existing bucket' do
      before do
        allow(storage).to receive(:bucket).with(bucket_name).and_return(bucket)
      end

      it 'returns the bucket object' do
        expect(described_class.connect_to_bucket(storage, bucket_name)).to eq bucket
      end
    end

    describe 'connecting to a nonexistent bucket' do
      before do
        allow(storage).to receive(:bucket).with(bucket_name).and_return(nil)
      end

      it 'raises an error' do
        expect { described_class.connect_to_bucket(storage, bucket_name) }
          .to raise_error(StandardError, "Bucket '#{bucket_name}' not found.")
      end
    end

    describe 'connecting to an invalid bucket name' do
      before do
        allow(storage).to receive(:bucket).with(bucket_name)
                                          .and_raise(Google::Cloud::InvalidArgumentError.new('Invalid bucket name'))
      end

      it 'raises an error' do
        expect { described_class.connect_to_bucket(storage, bucket_name) }
          .to raise_error(StandardError, "Invalid bucket name '#{bucket_name}'.")
      end
    end

    describe 'connecting to a permission-denied bucket' do
      before do
        allow(storage).to receive(:bucket).with(bucket_name)
                                          .and_raise(Google::Cloud::PermissionDeniedError.new('Invalid permissions'))
      end

      it 'raises an error' do
        expect { described_class.connect_to_bucket(storage, bucket_name) }
          .to raise_error(StandardError, "Permission denied for bucket '#{bucket_name}'.")
      end
    end
  end

  describe '.add_key_or_add' do
    let(:directory_sizes) { {} }
    let(:directory) { 'test' }

    context 'when the directory already exists in directory_sizes' do
      let(:size) { 50 }

      before do
        directory_sizes[directory] = 30
      end

      it 'updates the directory size with the new size' do
        expect(described_class.add_key_or_add(size, directory_sizes, directory)).to eq({ 'test' => 80 })
      end
    end

    context 'when the directory does not exist in directory_sizes' do
      let(:size) { 100 }

      it 'adds a new directory with the specified size' do
        expect(described_class.add_key_or_add(size, directory_sizes, directory)).to eq({ 'test' => 100 })
      end
    end
  end

  describe '.process_size_of_directories' do
    let(:bucket) { double('bucket') }
    let(:file1) { double('file', name: 'v1/test/dir1/file1.txt', size: 10) }
    let(:file2) { double('file', name: 'v1/test/dir2/file2.txt', size: 20) }
    let(:file3) { double('file', name: 'v1/test/file3.txt', size: 30) }
    let(:file4) { double('file', name: 'v2/test/dir1/file1.txt', size: 40) }
    let(:file5) { double('file', name: 'v2/test/dir2/file2.txt', size: 30) }
    let(:file6) { double('file', name: 'v2/test/file3.txt', size: 20) }

    before do
      allow(bucket).to receive_message_chain(:files,
                                             :all).and_yield(file1).and_yield(file2).and_yield(file3)
        .and_yield(file4).and_yield(file5).and_yield(file6)
    end

    it 'updates the directory sizes hash with the correct sizes' do
      expect(described_class.process_size_of_directories(bucket, '')).to eq({
                                                                              'v1' => 60,
                                                                              'v1/test' => 60,
                                                                              'v1/test/dir1' => 10,
                                                                              'v1/test/dir2' => 20,
                                                                              'v2' => 90,
                                                                              'v2/test' => 90,
                                                                              'v2/test/dir1' => 40,
                                                                              'v2/test/dir2' => 30
                                                                            })
    end
  end

  describe '.expected_vs_actual_dir_sizes' do
    let(:bucket) { 'my-bucket' }
    let(:directory_sizes) { { 'dir1' => 100, 'dir2' => 200, 'dir3' => 50 } }
    let(:expected_sizes) { { 'dir1' => 50, 'dir2' => 150, 'dir3' => 20 } }

    before do
      expect(described_class).to receive(:process_size_of_directories)
        .with(bucket)
        .and_return(directory_sizes)
    end

    it 'returns true when all directories have at least the expected size' do
      expect(described_class.expected_vs_actual_dir_sizes(bucket, expected_sizes)).to eq(true)
    end

    it 'returns false when at least one directory is below the expected size' do
      expected_sizes['dir1'] = 150 # modify the expected size to make the test fail
      expect(described_class.expected_vs_actual_dir_sizes(bucket, expected_sizes)).to eq(false)
    end
  end

  describe 'get_latest_timestamp' do
    let(:bucket) { double('bucket') }
    let(:object1) { double('object1') }
    let(:object2) { double('object2') }
    let(:object3) { double('object3') }

    let(:file_list1) { double('file_list1', token: 'next_page') }
    let(:file_list2) { double('file_list2', token: nil) }
    let(:file_list3) { double('file_list3', token: nil) }

    before do
      stub_const('LicenseDBSanityTest::MAX_FILES_PER_PAGE', 1)

      allow(file_list1).to receive(:each).and_yield(object1)
      allow(file_list2).to receive(:each).and_yield(object2)
      allow(file_list3).to receive(:each).and_yield(object3)

      allow(bucket).to receive(:files).with(prefix: 'v1', max: 1, token: nil).and_return(file_list1)
      allow(bucket).to receive(:files).with(prefix: 'v1', max: 1, token: 'next_page').and_return(file_list2)
      allow(bucket).to receive(:files).with(prefix: 'v2', max: 1, token: nil).and_return(file_list3)

      allow(object1).to receive(:updated_at).and_return(DateTime.now - 2)
      allow(object2).to receive(:updated_at).and_return(DateTime.now - 2)
      allow(object3).to receive(:updated_at).and_return(DateTime.now - 1)
    end

    it 'returns the timestamp of the most recent object in the bucket' do
      expect(described_class.get_latest_timestamp(bucket)).to eq(object3.updated_at)
    end
  end

  describe 'datetime_within_last_x_days?' do
    it 'returns true if the datetime is within the last x days' do
      datetime = DateTime.now - 1
      days = 2

      expect(described_class.datetime_within_last_x_days?(datetime, days)).to be_truthy
    end

    it 'returns false if the datetime is outside the last x days' do
      datetime = DateTime.now - 3
      days = 2

      expect(described_class.datetime_within_last_x_days?(datetime, days)).to be_falsey
    end
  end

  describe '.licenses_are_present?' do
    let(:bucket) { double('bucket') } # Mock the S3 bucket object
    let(:expected_subset) do
      [
        { name: 'License A', directory: 'licenses/a', default_licenses: ['MIT'], other_licenses: [] },
        { name: 'License B', directory: 'licenses/b', default_licenses: ['Apache-2.0'], other_licenses: ['GPLv3'] }
      ]
    end
    let(:file1) do
      double('file', name: "#{expected_subset.first[:directory]}/license.ndjson",
                     content: valid_ndjson(expected_subset.first))
    end
    let(:file2) do
      double('file', name: "#{expected_subset.last[:directory]}/license.ndjson",
                     content: valid_ndjson(expected_subset.last))
    end

    context 'when all licenses are present' do
      before do
        allow(bucket).to receive_message_chain(:files, :all).and_yield(file1).and_yield(file2)
        allow(file1).to receive(:download).and_return(StringIO.new(valid_ndjson(expected_subset.first)))
        allow(file2).to receive(:download).and_return(StringIO.new(valid_ndjson(expected_subset.last)))
      end

      it 'returns true' do
        expect(described_class.licenses_are_present?(bucket, expected_subset)).to be true
      end
    end

    context 'when a license is missing' do
      before do
        allow(bucket).to receive_message_chain(:files, :all).and_yield(file1)
        allow(file1).to receive(:download).and_return(StringIO.new(valid_ndjson(expected_subset.first)))
      end

      it 'returns false and prints a message' do
        expect do
          described_class.licenses_are_present?(bucket, expected_subset)
        end.to output(
          /License #{expected_subset.last[:name]} in directory #{expected_subset.last[:directory]} not found/
        ).to_stdout
        expect(described_class.licenses_are_present?(bucket, expected_subset)).to be false
      end
    end

    describe '.row_count_is_at_least_expected?' do
      let(:bucket_mock) { double('bucket') } # Mock bucket object

      context 'when all directories have expected row count' do
        let(:expected_rows) do
          [
            { directory: 'data/dir1', rows: 5 },
            { directory: 'data/dir2', rows: 5 }
          ]
        end
        let(:file1) do
          double('file', name: 'file1.csv')
        end
        let(:file2) do
          double('file', name: 'file2.csv')
        end
        let(:file3) do
          double('file', name: 'file3.csv')
        end

        let(:bucket_file_list_mock1) { double('BucketFileList') }
        let(:bucket_file_list_mock2) { double('BucketFileList') }

        before do
          # Define expected behavior for bucket.files (replace with your mocking logic)
          allow(bucket_mock).to receive(:files).with(prefix: 'data/dir1').and_return(bucket_file_list_mock1)
          allow(bucket_mock).to receive(:files).with(prefix: 'data/dir2').and_return(bucket_file_list_mock2)
          allow(bucket_file_list_mock1).to receive(:all).and_return([file1, file2])
          allow(bucket_file_list_mock2).to receive(:all).and_return([file3])

          # Mock file download content (replace with your mocking logic)
          allow(file1).to receive(:download).and_return(StringIO.new("Line 1\nLine 2\n"))
          allow(file2).to receive(:download).and_return(StringIO.new("Line 3\nLine 4\nLine 5\n"))
          allow(file3).to receive(:download).and_return(StringIO.new("Line 6\nLine 7\nLine 8\nLine 9\nLine 10"))
        end

        it 'returns true' do
          expect(described_class.row_count_is_at_least_expected?(bucket_mock, expected_rows)).to be true
        end
      end

      context 'when a directory has less than expected rows' do
        let(:expected_rows) do
          [
            { directory: 'data/dir1', rows: 100 }
          ]
        end
        let(:file1) do
          double('file', name: 'file1.csv')
        end
        let(:bucket_file_list_mock1) { double('BucketFileList') }

        before do
          allow(bucket_mock).to receive(:files).with(prefix: 'data/dir1').and_return(bucket_file_list_mock1)
          allow(bucket_file_list_mock1).to receive(:all).and_return([file1])
          allow(file1).to receive(:download).and_return(StringIO.new("Line 1\nLine 2\n"))
        end

        it 'returns false' do
          expect(described_class.row_count_is_at_least_expected?(bucket_mock, expected_rows)).to be false
        end
      end
    end

    private

    def valid_ndjson(license_hash)
      "{ \"name\": \"#{license_hash[:name]}\", \"default_licenses\": #{license_hash[:default_licenses].to_json},\
\"other_licenses\": #{license_hash[:other_licenses].to_json}}"
    end
  end
end
