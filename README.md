GitLab License Database Sanity Test Documentation

# Tests

Currently we perform a minimum size test against known directories in the License DB bucket. This proves out that the directories are both present and populated.

We also test that the latest timestamp on the Production bucket is within the past 2 days.

## Running tests

Run

`bundle install`

to populate required gems followed by

`TEST_BUCKET='<bucket-name>' bundle exec rspec ./qa/spec/sanity_test_exec_spec.rb`

## Unit tests

Unit tests of helper functions are ran by

`bundle install`

followed by

`bundle exec rspec ./spec/sanity_test_spec.rb`
