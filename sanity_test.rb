# frozen_string_literal: true

require 'google/cloud/storage'
require 'ndjson'

# Functions for License DB Sanity Test
module LicenseDBSanityTest

  MAX_FILES_PER_PAGE = 1000
  def self.connect_to_bucket(storage, bucket_name)
    bucket = storage.bucket(bucket_name)
    raise StandardError, "Bucket '#{bucket_name}' not found." if bucket.nil?

    bucket
  rescue Google::Cloud::InvalidArgumentError
    raise StandardError, "Invalid bucket name '#{bucket_name}'."
  rescue Google::Cloud::PermissionDeniedError
    raise StandardError, "Permission denied for bucket '#{bucket_name}'."
  rescue StandardError => e
    raise StandardError, e.message
  end

  def self.add_key_or_add(size, directory_sizes, directory)
    if directory_sizes.key?(directory)
      directory_sizes[directory] += size
    else
      directory_sizes[directory] = size
    end
    directory_sizes
  end

  # Recursive method to iterate over all the files in the bucket
  # and determine size of directories
  def self.process_size_of_directories(bucket, prefix = '')
    directory_sizes = {}
    bucket.files(prefix:).all do |file|
      # If the file is not a directory, extract the directory and file names, and file size
      components = file.name.split('/')
      directory = components[0..-2].join('/')
      # filename = components.last
      size = file.size

      # If the directory already exists in the hash, add the file size to its total size
      # Otherwise, create a new key for the directory and set its size to the file size
      add_key_or_add(size, directory_sizes, directory)

      # Check if the file belongs to a subdirectory, and add its size to the subdirectory's size
      if directory.include?('/')
        subdirectory = File.split(directory).first
        path_enum = Pathname.new(subdirectory).ascend
        path_enum.each { |path| add_key_or_add(size, directory_sizes, path.to_s) }
      end
    end
    directory_sizes
  end

  def self.expected_vs_actual_dir_sizes(bucket, expected_sizes)
    directory_sizes = process_size_of_directories(bucket)
    expected_sizes.each do |directory, size|
      puts "#{directory} is of size #{directory_sizes[directory]} expecting #{size}"
      if directory_sizes[directory] < size
        puts "#{directory} is not at least #{size}"
        return false
      end
    end
    puts 'Directories minimum size passed'
    true
  end

  def self.get_latest_timestamp(bucket)
    # Define prefixes to search
    prefixes = %w[v1 v2]
    latest_timestamp = nil

    prefixes.each do |prefix|
      next_page_token = nil
      loop do
        # Get next files page
        files = bucket.files(prefix:, max: MAX_FILES_PER_PAGE, token: next_page_token)
        files.each do |file|
          # Update latest_timestamp as needed
          if latest_timestamp.nil? || file.updated_at > latest_timestamp
            latest_timestamp = file.updated_at
          end
        end
        next_page_token = files.token
        # Stop iterating when there are no more pages
        break if next_page_token.nil?
      end
    end
    puts "latest_timestamp is: #{latest_timestamp}"
    latest_timestamp
  end

  def self.datetime_within_last_x_days?(datetime, days)
    current_time = DateTime.now
    time_diff = current_time - datetime
    time_diff <= days
  end

  def self.licenses_are_present?(bucket, expected_subset)
    expected_subset.each do |license_hash|
      found = false
      bucket.files(prefix: license_hash[:directory]).all do |file|
        filename = file.name
        # Check if it's a JSON file
        if filename.end_with?('.ndjson')
          ndjson_stream = file.download
          parser = NDJSON::Parser.new(ndjson_stream)
          parser.each do |entry|
            next unless entry['name'] == license_hash[:name]

            next unless entry['default_licenses'].to_json == license_hash[:default_licenses].to_json &&
                        entry['other_licenses'].to_json == license_hash[:other_licenses].to_json

            found = true
            break
          end
          break if found
        end
      end
      unless found
        puts "License #{license_hash[:name]} in directory #{license_hash[:directory]} not found"
        return found
      end
    end
    true
  end

  def self.row_count_is_at_least_expected?(bucket, expected_rows)
    # Flag to indicate success (initially true)
    success = true

    threads = []

    # Iterate through each directory and expected row count
    expected_rows.each do |expect_hash|
      threads << Thread.new do
        files = bucket.files(prefix: expect_hash[:directory]).all
        puts "Checking directory #{expect_hash[:directory]}"

        actual_count = 0
        files.each do |file|
          next unless file.name.end_with?('.csv') || file.name.end_with?('.ndjson')

          content = file.download.read

          actual_count += content.lines.count
        end

        success = false if actual_count < expect_hash[:rows]

        unless success
          puts "Error: Directory #{expect_hash[:directory]}, Expected #{expect_hash[:rows]} rows, "\
                 "Found #{actual_count} rows"
        end
      end
    end
    # Wait for all threads to finish
    threads.each(&:join)

    success
  end
end
