# frozen_string_literal: true

require_relative '../../sanity_test_exec'

RSpec.describe LicenseDBSanityTestExec do
  let(:bucket_name) do
    ENV['TEST_BUCKET']
  end

  describe 'Directory Sizes' do
    let(:expected_sizes) do
      {
        'v1/rubygem' => 35_000_000,
        'v1/pypi' => 100_000_000,
        'v1/packagist' => 150_000_000,
        'v1/nuget' => 230_000_000,
        'v1/npm' => 1_100_000_000,
        'v1/maven' => 650_000_000,
        'v1/go' => 1_100_000_000,
        'v1/conan' => 110_000,
        'v1' => 3_800_000_000,
        'v2/rubygem' => 22_000_000,
        'v2/pypi' => 68_000_000,
        'v2/packagist' => 45_000_000,
        'v2/nuget' => 91_000_000,
        'v2/npm' => 596_000_000,
        'v2/maven' => 100_000_000,
        'v2/go' => 400_000_000,
        'v2/conan' => 110_000,
        'v2/cargo' => 110_000,
        'v2/swift' => 110_000,
        'v2' => 1_500_000_000
      }
    end

    it 'are at least the expected sizes' do
      expect(LicenseDBSanityTestExec.directories_are_of_expected_size(bucket_name, expected_sizes)).to eq(true)
    end
  end

  describe 'Latest Timestamp' do
    it 'is within the past 2 days' do
      # TODO: If dev bucket has frequent exports, remove this skip
      skip 'Test not applicable in dev environment' if bucket_name =~ /^dev-/
      expect(LicenseDBSanityTestExec.timestamp_within_past_two_days(bucket_name)).to eq(true)
    end
  end

  describe 'Expected number of rows' do
    let(:expected_rows) do
      [
        { directory: 'v2/go',
          rows: 4_987_000 },
        {
          directory: 'v2/conan',
          rows: 7000
        },
        {
          directory: 'v2/maven',
          rows: 3_310_000
        },
        {
          directory: 'v2/npm',
          rows: 12_045_000
        },
        {
          directory: 'v2/nuget',
          rows: 2_751_000
        },
        {
          directory: 'v2/packagist',
          rows: 1_445_000
        },
        {
          directory: 'v2/pypi',
          rows: 2_372_000
        },
        {
          directory: 'v2/rubygem',
          rows: 728_000
        }
      ]
    end
    it 'are at least present' do
      # skip 'Test not executed in dev environment' if bucket_name =~ /^dev-/
      skip 'Skipping due to https://gitlab.com/gitlab-org/security-products/tests/license-db-sanity/-/issues/5'

      expect(LicenseDBSanityTestExec.expected_number_of_rows(bucket_name, expected_rows)).to eq(true)
    end
  end

  describe 'Subset of licenses' do
    let(:expected_subset_licenses) do
      [
        { directory: 'v2/go',
          name: 'github.com/vine-io/vine',
          default_licenses: ['MIT'],
          other_licenses: [{ "licenses": ['Apache-2.0'],
                             "versions":
                              %w[v0.10.0 v0.10.1 v0.10.11 v0.10.12 v0.10.13 v0.10.14 v0.10.15 v0.10.16 v0.10.17
                                 v0.10.18 v0.10.19 v0.10.2 v0.10.21 v0.10.3 v0.10.4 v0.10.6 v0.10.8 v0.10.9 v0.11.0
                                 v0.12.0 v0.12.1 v0.5.0 v0.6.1 v0.6.3 v0.7.0 v0.7.1 v0.7.2 v0.8.2 v0.8.3 v0.8.4
                                 v0.9.4] },
                           { "licenses": ['unknown'], "versions": %w[v0.2.2 v0.2.3] }] },
        { directory: 'v2/conan',
          name: 'openssl',
          default_licenses: ['OpenSSL'],
          other_licenses: [{ 'licenses': ['Apache-2.0'], 'versions': %w[3.0.4 3.0.5 3.0.7 3.0.8 3.0.9 3.1.0 3.1.1] }] },
        {
          directory: 'v2/maven',
          name: 'org.springframework.ldap/spring-ldap-odm',
          default_licenses: ['Apache-2.0'],
          other_licenses: [
            { 'licenses': ['Apache-2.0'],
              'versions': %w[1.3.2.RELEASE 2.0.0.RELEASE 2.0.1.RELEASE 2.0.2.RELEASE 2.0.3.RELEASE 2.0.4.RELEASE
                             2.1.0.RELEASE 2.2.0.RELEASE 2.2.1.RELEASE 2.3.0.RELEASE 2.3.1.RELEASE 2.3.2.RELEASE
                             2.3.3.RELEASE 2.3.4.RELEASE 2.3.5.RELEASE 2.3.6.RELEASE 2.3.7.RELEASE 2.3.8.RELEASE] },
            { 'licenses': ['unknown'], 'versions': ['1.3.1.RELEASE'] }
          ]
        },
        {
          directory: 'v2/npm',
          name: 'matterbridge-zigbee2mqtt',
          default_licenses: ['Apache-2.0'],
          other_licenses: [{ "licenses": ['MIT'], "versions": %w[1.0.8 1.0.9] }]
        },
        {
          directory: 'v2/nuget',
          name: 'NINA.Image',
          default_licenses: ['MPL-2.0'],
          other_licenses: [{ 'licenses': %w[MPL-2.0 MPL-2.0-no-copyleft-exception],
                             'versions': %w[3.0.0.1001-nightly 3.0.0.1001-nightly2 3.0.0.1001-nightly3
                                            3.0.0.1001-preview2 3.0.0.1001-preview3
                                            3.0.0.1001-preview4 3.0.0.1006-nightly 3.0.0.1008-nightly
                                            3.0.0.1010-nightly 3.0.0.1015-nightly
                                            3.0.0.1021-nightly 3.0.0.1022-nightly 3.0.0.1031-nightly
                                            3.0.0.1032-nightly 3.0.0.1056-nightly] }]
        },
        {
          directory: 'v2/packagist',
          name: 'laeawa/bephp-composer-hello',
          default_licenses: ['unknown'],
          other_licenses: [{ "licenses": ['unknown'], 'versions': ['dev-master'] }]
        },
        {
          directory: 'v2/pypi',
          name: 'ssss',
          default_licenses: ['unknown'],
          other_licenses: [{ "licenses": %w[GPL-3.0-or-later LGPL-3.0+], "versions": ['0.1'] }]
        },
        {
          directory: 'v2/rubygem',
          name: 'AsposeDiagramCloud',
          default_licenses: ['MIT'],
          other_licenses: [{ 'licenses': ['unknown'], 'versions': ['20.3'] }]
        }
      ]
    end

    it 'are present' do
      skip 'Skipping due to https://gitlab.com/gitlab-org/security-products/tests/license-db-sanity/-/issues/5'
      expect(LicenseDBSanityTestExec.subset_of_licenses_are_present(bucket_name, expected_subset_licenses)).to eq(true)
    end
  end
end
